<?php
session_start(); //一旦有使用到暫存 必須要寫session_start()
require("prdModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php"); //header跳轉業面的指令
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>

<body>
<p>This is the MAIN page 
[<a href="logout.php">logout</a>]

</p>
<hr>
<?php
    //一旦有使用到暫存 最上面記得要寫session_start()歐
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getPrdList();
?>
<a href="order.show.php">List My Orders</a><hr>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>name</td>
    <td>price</td>
    <td>+</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['prdID'] . "</td>"; //第一種寫法""
	echo "<td>{$rs['name']}</td>"; //第二種寫法
	echo "<td>" , $rs['price'], "</td>";
	echo "<td><a href='Cart.addItem.php?prdID=" , $rs['prdID'] , "'>Add</a></td></tr>";
}
?>
</table>
<a href="Cart.showDetail.php">Show Shopping Cart Content</a><hr>

</body>
</html>
